#include <sys/select.h>
#if defined(_WIN32)
#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif
#include <winsock2.h>
#include <ws2_tcpip.h>
#pragma comment(lib, "ws2_32.lib");

#else
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#endif

#if defined(_WIN32)
#define GETSOCKETERRNO() (WSAGetLastError())
#define CLOSESOCKET(s) closesocket(s)
#define ISVALIDSOCKET(s) ((s) != INVALID_SOCKET)

#else
#define GETSOCKETERRNO() (errno)
#define ISVALIDSOCKET(s) ((s) >= 0)
#define SOCKET int
#define CLOSESOCKET(s) close(s)

#endif
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char **argv) {
#if defined(_WIN32)
  WSADATA ws;
  if (WSAStartup(MAKEWORD(2, 2), &ws)) {
    printf("WSAStartup() error\n");
    return -1;
  }
#endif

  struct addrinfo host_addr;

  host_addr.ai_socktype = SOCK_STREAM;
  host_addr.ai_flags = AI_PASSIVE;
  host_addr.ai_family = AF_INET;

  struct addrinfo *bind_addr;

  getaddrinfo(0, "8888", &host_addr, &bind_addr);

  SOCKET socket_main = socket(bind_addr->ai_family, bind_addr->ai_socktype,
                              bind_addr->ai_protocol);
  if (!ISVALIDSOCKET(socket_main)) {
    fprintf(stderr, "Socket() error, (%d)\n", GETSOCKETERRNO());
    return -2;
  }
  if (bind(socket_main, bind_addr->ai_addr, bind_addr->ai_addrlen)) {
    fprintf(stderr, "bind() error (%d)\n", GETSOCKETERRNO());
    return -3;
  }
  freeaddrinfo(bind_addr);
  if (listen(socket_main, 10)) {
    fprintf(stderr, "listen() error,(%d)\n", GETSOCKETERRNO());
    return -4;
  }
  fd_set master;
  FD_ZERO(&master);
  FD_SET(socket_main, &master);
  int max_socket = socket_main;
  while (66) {
    fd_set connections;
    connections = master;
    if (select(max_socket + 1, &connections, 0, 0, 0) < 0) {
      fprintf(stderr, "select() error %d\n", GETSOCKETERRNO());
      return -5;
    }
    for (int i = 1; i <= max_socket; i++) {
      if (FD_ISSET(i, &connections)) {
        if (i == socket_main) {
          struct sockaddr_storage client_addr;
          socklen_t client_size = sizeof(client_addr);

          SOCKET new_connection =
              accept(i, (struct sockaddr *)&client_addr, &client_size);
          if (!ISVALIDSOCKET(new_connection)) {
            fprintf(stderr, "accept() error, (%d)\n", GETSOCKETERRNO());
            return -6;
          }
          FD_SET(new_connection, &master);
          max_socket = new_connection;
        } else {
          char buffer[4096] = {'\0'};
          int received_bytes = recv(i, buffer, sizeof(buffer), 0);
          if (received_bytes < 1) {
            printf("Connection interrupted\n");
            FD_CLR(i, &master);
            CLOSESOCKET(i);
            continue;
          }
          switch (buffer[0]) {
          case 's':
            FILE *read = fopen("task_list.txt", "r");
            if (read == NULL) {
              send(i, "file not created,error",
                   sizeof("file not created, error"), 0);
              break;
            }
            char temp[1024];
            while (!feof(read)) {
              fread(temp, sizeof(temp), 1, read);
              send(i, temp, sizeof(temp), 0);
            }
            send(i, temp, sizeof(temp), 0);
            fclose(read);
            break;
          case 'w':
            FILE *write = fopen("task_list.txt", "a+");
            if (write == NULL) {
              printf("fopen error, (%d)\n", GETSOCKETERRNO());
            }
            buffer[0] = ' ';
            fwrite(buffer, 1, strlen(buffer), write);
            fclose(write);
            break;

          case 'c':
            remove("task_list.txt");
            break;

          case 'q':
            FD_CLR(i, &master);
            CLOSESOCKET(i);

            break;

          default:
            break;
          }
          for (int j = 0; j < strlen(buffer); j++) {
            buffer[j] = toupper(buffer[j]);
          }
          send(i, buffer, sizeof(buffer), 0);
        }
      }
    }
    FD_CLR(socket_main, &master);
    CLOSESOCKET(socket_main);
#if defined(_WIN32)
    WSAClenup();
#endif
    return 0;
  }
