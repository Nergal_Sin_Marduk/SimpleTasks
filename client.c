#include "crossplatform.h"
#include <memory.h>
#include <stdio.h>

#if defined(_WIN32)
#include <conio.h>
#endif

void help() {
  puts("For write use {w <string>}");
  puts("For read use {r <string not will be execute>}");
  puts("For clean the filelist use {c <string will be not execute}");
  puts("For quit use {q}");
  puts("\n\n\n");
}

int main(int argc, char **argv) {
#if defined(_WIN32)
  WSADATA ws;
  if (WSAStartup(MAKEWORD(2, 2), &ws)) {
    printf("WSAStartup() error, \n");
    return -1;
  }
#endif

  const char *PORT = "YOUR_LOCAL_PEER_PORT";
  const char *IP = "YOUR_LOCAL_PEER_IP";

  if (argc < 3) {
    printf("usage: client <ip> <port>\n");
    printf("returning to common settings...\n");
  }
  struct addrinfo client_addr;
  struct addrinfo *bind_client;
  client_addr.ai_socktype = SOCK_STREAM;

  help();
  if (argc < 3)
    getaddrinfo(IP, PORT, &client_addr, &bind_client);
  else {
    getaddrinfo(argv[1], argv[2], &client_addr, &bind_client);
  }
  SOCKET sock_connect = socket(bind_client->ai_family, bind_client->ai_socktype,
                               bind_client->ai_protocol);
  if (!ISVALIDSOCKET(sock_connect)) {
    fprintf(stderr, "socket() error, (%d)\n", GETSOCKETERRNO());
    return -2;
  }
  if (connect(sock_connect, bind_client->ai_addr, bind_client->ai_addrlen)) {
    fprintf(stderr, "connect() error, (%d)\n", GETSOCKETERRNO());
    return -3;
  }
  freeaddrinfo(bind_client);

  int max_connect = sock_connect;
  while (66) {
    fd_set streams;
    FD_ZERO(&streams);
    FD_SET(sock_connect, &streams);
#if !defined(_WIN32)
    FD_SET(0, &streams);
#endif

    struct timeval timeout;
    timeout.tv_sec = 1;
    if (select(max_connect + 1, &streams, 0, 0, &timeout) < 0) {
      fprintf(stderr, "select() error,(%d)\n", GETSOCKETERRNO());
      return -4;
    }
    if (FD_ISSET(sock_connect, &streams)) {
      char recv_buffer[4096];
      memset(recv_buffer, 0, sizeof(recv_buffer));
      int receive_bytes =
          recv(sock_connect, &recv_buffer, sizeof(recv_buffer), 0);
      if (receive_bytes < 1) {
        printf("Connection ended by peer\n");
        FD_CLR(sock_connect, &streams);
        CLOSESOCKET(sock_connect);
        return 0;
      }
      printf("%s:\n\t%.*s\n", IP, receive_bytes, recv_buffer);
    }
#if defined(_WIN32)
    if (_kbhit()) {
#else
    if (FD_ISSET(0, &streams)) {
#endif
      char sent_buffer[4096];
      memset(sent_buffer, 0, sizeof(sent_buffer));
      if (!fgets(sent_buffer, sizeof(sent_buffer), stdin)) {
        fprintf(stderr, "fgets() error,(%d)\n", GETSOCKETERRNO());
        return -5;
      }
      send(sock_connect, &sent_buffer, strlen(sent_buffer), 0);
    }
  }
#if defined(_WIN32)
  WSACleanup();
#endif
  return 0;
}
